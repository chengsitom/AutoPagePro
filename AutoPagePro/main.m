//
//  main.m
//  AutoPagePro
//
//  Created by 程思园(EX-CHENGSIYUAN001) on 2018/6/19.
//  Copyright © 2018年 程思园(EX-CHENGSIYUAN001). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
