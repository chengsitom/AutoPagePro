//
//  ViewController.m
//  AutoPagePro
//
//  Created by 程思园(EX-CHENGSIYUAN001) on 2018/6/19.
//  Copyright © 2018年 程思园(EX-CHENGSIYUAN001). All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor orangeColor];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
